#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "helpers.h"

const int usno_col_per_line = 188;
const int usno_MJD0 = 48622;
const int iers_col_per_line = 156;
const int iers_MJD0 = 37665;
const int iers_start_offset = 672;

const char *usno_file_path = "finals2000A.data";
const char *iers_file_path = "eopc04_IAU2000.txt";
const int days_max = 20;
const int param_cnt = 6;

double eop_table[ days_max * param_cnt ];
// EOP Table
// 1 - MJD
// 2 - X Pole (sec. of arc)
// 3 - Y Pole (sec. of arc)
// 4 - dUT1 (sec)
// 5 - dX (msec. of arc)
// 5 - dY (msec. of arc)

double *eop_MJD  = eop_table;
double *eop_Xp   = eop_table + days_max;
double *eop_Yp   = eop_table + days_max*2;
double *eop_dUT1 = eop_table + days_max*3;
double *eop_dX   = eop_table + days_max*4;
double *eop_dY   = eop_table + days_max*5;



unsigned read_eops_usno( double MJD )
{
	char readBuf[2048];
	
	char *archive = getenv( "GNSS_ARC" );
	if ( archive == NULL )
		return 0;

	strcpy( readBuf, archive );
	strcat( readBuf, usno_file_path );	

	FILE *inpFile = fopen( readBuf, "rt" );

	if ( inpFile == NULL )
		return 0;

	int MJD_min = int(MJD) - 4;
	int MJD_max = int(MJD) + days_max - 5;

	long offset = ( MJD_min - usno_MJD0 ) * usno_col_per_line;
	fseek( inpFile, offset, SEEK_SET );

	const size_t data_total = 11;
	const size_t data_off[] = { 7, 18, 37, 58, 97, 116, 134, 144, 154, 165, 175 };
	const size_t data_len[] = { 8,  9,  9, 10,  9,   9,  10,  10,  11,  10,  10 };

	double record[data_total];

	double MJD_cur = 0.0;
	unsigned len = 0;
	unsigned recCnt = 0;

	while ( fgets( readBuf, 2048, inpFile ) ) {

		katof( readBuf + data_off[0], &MJD_cur, data_len[0], data_len[0] );
		if ( MJD_cur < MJD_min )
			continue;
		if ( MJD_cur > MJD_max )
			break;

		len = strlen( readBuf );

		if ( len < iers_col_per_line )
			break;

		for ( int i = 0; i < data_total; ++i ) {
			katof( readBuf + data_off[i], &record[i], data_len[i], data_len[i] );
		}

		eop_MJD [recCnt] = record[0];
		
		eop_Xp  [recCnt] = (record[6]  == 0.0) ? record[1] : record[6];
		eop_Yp  [recCnt] = (record[7]  == 0.0) ? record[2] : record[7];
		eop_dUT1[recCnt] = (record[8]  == 0.0) ? record[3] : record[8];
		eop_dX  [recCnt] = (record[9]  == 0.0) ? record[4] : record[9];
		eop_dY  [recCnt] = (record[10] == 0.0) ? record[5] : record[10];

		++recCnt;
		
	}

	fclose( inpFile );

	return recCnt;
}

unsigned read_eops_iers( double MJD )
{
	char readBuf[2048];

	char *archive = getenv( "GNSS_ARC" );
	if ( archive == NULL )
		return 0;

	strcpy( readBuf, archive );
	strcat( readBuf, iers_file_path );	

	FILE *inpFile = fopen( readBuf, "rt" );

	if ( inpFile == NULL )
		return 0;

	int MJD_min = int(MJD) - 4;
	int MJD_max = int(MJD) + days_max - 5;

	long offset = ( MJD_min - iers_MJD0 ) * iers_col_per_line + iers_start_offset;
	fseek( inpFile, offset, SEEK_SET );

	const size_t data_total = 6;
	const size_t data_off[] = { 12, 19, 30, 41, 65, 76 };
	const size_t data_len[] = {  7, 11, 11, 12, 11, 11 };

	double record[data_total];

	double MJD_cur = 0.0;
	unsigned len = 0;
	unsigned recCnt = 0;

	while ( fgets( readBuf, 2048, inpFile ) ) {

		katof( readBuf + data_off[0], &MJD_cur, data_len[0], data_len[0] );
		if ( MJD_cur < MJD_min )
			continue;
		if ( MJD_cur > MJD_max )
			break;

		len = strlen( readBuf );

		if ( len < iers_col_per_line )
			break;

		for ( int i = 0; i < data_total; ++i ) {
			katof( readBuf + data_off[i], &record[i], data_len[i], data_len[i] );
		}

		eop_MJD [recCnt] = record[0];
		
		eop_Xp  [recCnt] = record[1];
		eop_Yp  [recCnt] = record[2];
		eop_dUT1[recCnt] = record[3];
		eop_dX  [recCnt] = record[4] * 1000;
		eop_dY  [recCnt] = record[5] * 1000;

		++recCnt;
		
	}

	fclose( inpFile );

	return recCnt;
}

//unsigned pick_iers_bull( unsigned year, unsigned month, char *iersFile )
//{
//	const unsigned iersYear0 = 2009;
//	const unsigned iersBull0 = 253;
//
//	if ( year < iersYear0 )
//		return 0;
//
//	year -= iersYear0;
//
//	unsigned bull_num = iersBull0 + year * 12 + month - 1;
//
//	char bull_num_s[100];
//
//	itoa( bull_num, bull_num_s, 10 );
//
//	strcpy( iersFile, "..\\lib\\data\\bullb\\bulletinb-" );
//	strcat( iersFile, bull_num_s );
//	strcat( iersFile, ".txt" );
//
//	return bull_num;
//}
//
//unsigned read_iers_bull( const char *iersFile, double MJD[], double Xp[], double Yp[], double dUT1[], double dX[], double dY[] )
//{
//	FILE *inpFile = fopen( iersFile, "rt" );
//
//	if ( inpFile == NULL )
//		return 0;
//
//	char readBuf[500];
//
//	unsigned recCnt = 0;
//
//	bool inSection = false;
//
//	const size_t data_total = 6;
//	const size_t data_len[] = {  8,  9,  9, 10,  9,  8 };
//	const size_t data_off[] = { 13, 21, 30, 39, 49, 58 };
//
//	double record[data_total];
//
//
//	while ( fgets( readBuf, 500, inpFile ) ) {
//
//		if ( !inSection ) {
//			if ( strstr( readBuf, "1 - DAILY FINAL VALUES" ) != NULL ) {
//				inSection = true;
//			}
//
//			continue;
//		}
//
//		if ( inSection && readBuf[0] != '2' && strstr( readBuf, "2 - DAILY FINAL VALUES" ) != NULL )
//			break;
//
//		if ( inSection && readBuf[0] == '2' ) {
//			for ( int i = 0; i < data_total; ++i )
//				katof( readBuf + data_off[i], &record[i], data_len[i], data_len[i] );
//
//			MJD [recCnt] = record[0];
//			Xp  [recCnt] = record[1];
//			Yp  [recCnt] = record[2];
//			dUT1[recCnt] = record[3];
//			dX  [recCnt] = record[4];
//			dY  [recCnt] = record[5];
//
//			++recCnt;
//		}
//	}
//
//	fclose( inpFile );
//
//	return recCnt;
//}
//
//unsigned read_eops( double MJD, unsigned year, unsigned month, double MJDs[], double Xp[], double Yp[], double dUT1[], double dX[], double dY[] )
//{
//	char iersFile[100];
//	
//	unsigned bull_n = pick_iers_bull( year, month, iersFile );
//	if ( bull_n == 0 )
//		return 0;
//
//	unsigned rec_cnt = read_iers_bull( iersFile, MJDs, Xp, Yp, dUT1, dX, dY );
//
//	
//	bool eopsFound = false;
//
//	if ( rec_cnt == 0 )						// Try previous bulletin
//		--month;
//	else if ( MJDs[0] >= MJD - 1 )			// Try previous bulletin
//		--month;
//	else if ( MJDs[rec_cnt-1] <= MJD + 2 )	// Try next bulletin
//		++month;
//	else
//		eopsFound = true;
//
//	if ( !eopsFound ) {
//
//		bull_n = pick_iers_bull( year, month, iersFile );
//
//		if ( bull_n == 0 )
//			return 0;
//
//		rec_cnt = read_iers_bull( iersFile, MJDs, Xp, Yp, dUT1, dX, dY );
//
//		if ( rec_cnt == 0 || MJDs[0] >= MJD - 1 || MJDs[rec_cnt-1] <= MJD + 2 )
//			return 0;
//
//		eopsFound = true;
//	}
//
//	if ( eopsFound )
//		return rec_cnt;
//	else
//		return 0;
//}
