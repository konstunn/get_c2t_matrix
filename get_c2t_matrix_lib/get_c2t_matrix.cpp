#include "sofa.h"
#include "helpers.h"
#include "get_c2t_matrix.h"

// Arcseconds to radians
const double AS2R = 4.848136811095359935899141e-6;
const double R2AS = 206264.8062470963551564734;

extern double *eop_MJD;
extern double *eop_Xp;
extern double *eop_Yp;
extern double *eop_dUT1;
extern double *eop_dX;
extern double *eop_dY;

int eop_Cnt = 0;

unsigned read_eops_usno( double MJD );
unsigned read_eops_iers( double MJD );
void INTERP( double *EOPs, double RJD[], double X[], double Y[], double T[], double DX[], double DY[], int N, double RJDINT, double &XINT, double &YINT, double &TINT, double &DXINT, double &DYINT );
void get_eop_partials( double time_diff, double XP, double YP, double ERA, double SP, double RPOM[3][3], double RC2I[3][3], EOP_PARTIALS *part );

int get_c2t_matrix( double UTC, double c2t_matrix[3][3], double pole[2], double *EOPs, EOP_PARTIALS *part )
{
	static double mjdInit = 0.0;

	int IY, IM, ID;
	double SEC;
	double XP, YP, DUT1;
	double DX06, DY06;
	double TIME, TT, TAI, UT1, TUT, DAT;
	double DJMJD0, DATE;
	double RC2TI[3][3], RPOM[3][3], RC2I[3][3], RC2IT[3][3];
	double X, Y, S;
	double ERA;
	double time_diff;

	
	DATE = myround( UTC );
	DJMJD0 = 2400000.5;

	// TT (MJD).
	TIME = UTC - DATE;
	iauJd2cal( DJMJD0, DATE, &IY, &IM, &ID, &SEC ); 
	iauDat( IY, IM, ID, TIME, &DAT );
	TAI = UTC + DAT/86400;
	TT = TAI + 32.184/86400;

	if ( abs( DATE - mjdInit ) > 2.0 ) {
		eop_Cnt = read_eops_usno( DATE );
		
		if ( eop_Cnt == 0 )
			return 0;

		mjdInit = DATE + 0.5;
	}

	if ( EOPs )
		time_diff = UTC - EOPs[0];
	else
		time_diff = UTC - (DATE + 0.5);

	// eop_Xp, eop_Yp = ARCSEC
	// eop_dX, eop_dY = MAS
	// eop_dUT1 = SEC
    
	// Calculating EOPs
	INTERP( EOPs, eop_MJD, eop_Xp, eop_Yp, eop_dUT1, eop_dX, eop_dY, eop_Cnt, UTC, XP, YP, DUT1, DX06, DY06 );

	pole[0] = XP * 1000;
	pole[1] = YP * 1000;

	// Polar motion (as->radians).
	XP *= AS2R;
	YP *= AS2R;
	// Celestial pole correction (mas->radians).
	DX06 *= AS2R/1000;
	DY06 *= AS2R/1000;

	// UT1.
	TUT = TIME + DUT1/86400;
	UT1 = DATE + TUT;

	// =========================
	// IAU 2006/2000A, CIO based
	// =========================
	// CIP and CIO, IAU 2006/2000A.
	iauXys06a( DJMJD0, TT, &X, &Y, &S );
	// Add CIP corrections.
	X = X + DX06;
	Y = Y + DY06;
	// GCRS to CIRS matrix.
	iauC2ixys( X, Y, S, RC2I );
	// Earth rotation angle.
	ERA = iauEra00 ( DJMJD0+DATE, TUT );
	// Form celestial-terrestrial matrix (no polar motion yet).
	iauCr( RC2I, RC2TI );
	iauRz( ERA, RC2TI );
	// Polar motion matrix (TIRS->ITRS, IERS 2003).
	iauPom00( XP, YP, iauSp00(DJMJD0,TT), RPOM );
	// Form celestial-terrestrial matrix (including polar motion).
	iauRxr( RPOM, RC2TI, c2t_matrix );

	if ( part )
		get_eop_partials( time_diff, XP, YP, ERA, iauSp00(DJMJD0,TT), RPOM, RC2I, part );

	return 1;
}

// Calculation of EOP partials from GAMIT (orbits\eopart.f)
void get_eop_partials( double time_diff, double XP, double YP, double ERA, double SP, double RPOM[3][3], double RC2I[3][3], EOP_PARTIALS *part )
{
	double RC2TI[3][3], temp[3][3];

	iauZr( part->xpole_p );
	iauZr( part->xpole_rate_p );
	iauZr( part->ypole_p );
	iauZr( part->ypole_rate_p );
	iauZr( part->ut1_rate_p );

	////////////////////////////////////////////////////
	// Calculating Y pole and Y pole rate derivatives //
	////////////////////////////////////////////////////

	iauIr( temp );
	iauRx( YP, temp );

	part->xpole_p[0][0] = -sin(XP);
	part->xpole_p[0][2] = -cos(XP);
	part->xpole_p[2][0] =  cos(XP);
	part->xpole_p[2][2] = -sin(XP);

	iauRxr( part->xpole_p, temp, part->xpole_p );
	iauRz( -SP, part->xpole_p );

	part->xpole_rate_p[0][0] = -sin(XP) * time_diff;
	part->xpole_rate_p[0][2] = -cos(XP) * time_diff;
	part->xpole_rate_p[2][0] =  cos(XP) * time_diff;
	part->xpole_rate_p[2][2] = -sin(XP) * time_diff;

	iauRxr( part->xpole_rate_p, temp, part->xpole_rate_p );
	iauRz( -SP, part->xpole_rate_p );

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////	


	////////////////////////////////////////////////////
	// Calculating Y pole and Y pole rate derivatives //
	////////////////////////////////////////////////////
	
	part->ypole_p[1][1] = -sin(YP);
	part->ypole_p[1][2] =  cos(YP);
	part->ypole_p[2][1] = -cos(YP);
	part->ypole_p[2][2] = -sin(YP);

	iauRy(  XP, part->ypole_p );
	iauRz( -SP, part->ypole_p );

	part->ypole_rate_p[1][1] = -sin(YP) * time_diff;
	part->ypole_rate_p[1][2] =  cos(YP) * time_diff;
	part->ypole_rate_p[2][1] = -cos(YP) * time_diff;
	part->ypole_rate_p[2][2] = -sin(YP) * time_diff;

	iauRy(  XP, part->ypole_rate_p );
	iauRz( -SP, part->ypole_rate_p );

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////


	////////////////////////////////////////////////////
	// Calculating UT1 rate derivative                //
	////////////////////////////////////////////////////

	part->ut1_rate_p[0][0] = -sin(-ERA) * time_diff;
	part->ut1_rate_p[0][1] =  cos(-ERA) * time_diff;
	part->ut1_rate_p[1][0] = -cos(-ERA) * time_diff;
	part->ut1_rate_p[1][1] = -sin(-ERA) * time_diff;

	////////////////////////////////////////////////////
	////////////////////////////////////////////////////

	// Finish calculating XY pole and pole rate derivatives
	iauRz( -ERA, part->xpole_p );
	iauRz( -ERA, part->ypole_p );
	iauRz( -ERA, part->xpole_rate_p );
	iauRz( -ERA, part->ypole_rate_p );

	iauTr( RC2I, RC2TI );

	iauRxr( RC2TI, part->xpole_p, part->xpole_p );
	iauRxr( RC2TI, part->xpole_rate_p, part->xpole_rate_p );
	iauRxr( RC2TI, part->ypole_p, part->ypole_p );
	iauRxr( RC2TI, part->ypole_rate_p, part->ypole_rate_p );

	// Finish calculating UT1 rate derivative
	iauIr( temp );
	iauRx(  YP, temp );
	iauRy(  XP, temp );
	iauRz( -SP, temp );

	iauRxr( part->ut1_rate_p, temp, part->ut1_rate_p );
	iauRxr( RC2TI, part->ut1_rate_p, part->ut1_rate_p );
}