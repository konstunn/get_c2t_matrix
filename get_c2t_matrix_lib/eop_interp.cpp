#include <math.h>

void PMUT1_OCEANS( double rjd, const double ARG[6], const double DARG[6], double &cor_x, double &cor_y, double &cor_ut1, double &cor_lod );
void PM_GRAVI( double rjd, const double ARG[6], double &cor_x, double &cor_y );
void UTLIBR( double RMJD, const double ARG[6], double &cor_ut1, double &cor_lod );
double LAGINT ( double X[], double Y[], int N,  double XINT );
void FUNDARG( double rjd, double ARG[6], double DARG[6] );
void FCNNUT( double MJD, double &dX, double &dY );

void INTERP( double *EOPs, double MJD[], double X[], double Y[], double T[], double DX[], double DY[], int N, double mjd_int, double &x_int, double &y_int, double &ut1_int, double &dX_int, double &dY_int )
{
//
//     THIS SUBROUTINE TAKES A SERIES OF X, Y, dX, dY AND UT1-UTC VALUES
//     AND INTERPOLATES THEM TO AN EPOCH OF CHOICE.  THIS ROUTINE
//     ASSUMES THAT THE VALUES OF X AND Y ARE IN SECONDS OF
//     ARC AND THAT UT1-UTC IS IN SECONDS OF TIME.  AT LEAST
//     ONE POINT BEFORE AND ONE POINT AFTER THE EPOCH OF THE
//     INTERPOLATION POINT ARE NECESSARY IN ORDER FOR THE
//     INTERPOLATION SCHEME TO WORK.
//
//     PARAMETERS ARE :
//     RJD   - ARRAY OF THE EPOCHS OF DATA (GIVEN IN MJD)
//     X     - ARRAY OF X POLAR MOTION (ARCSEC)
//     Y     - ARRAY OF Y POLAR MOTION (ARCSEC)
//     T     - ARRAY OF UT1-UTC (SEC)
//     DX    - ARRAY OF dX CELESTIAL POLE OFFSET (MAS)
//     DY    - ARRAY OF dY CELESTIAL POLE OFFSET (MAS)
//     N     - NUMBER OF POINTS IN ARRAYS
//     RJDINT- EPOCH FOR THE INTERPOLATED VALUE
//     XINT  - INTERPOLATED VALUE OF X (ARCSEC)
//     YINT  - INTERPOLATED VALUE OF Y (ARCSEC)
//     TINT  - INTERPOLATED VALUE OF UT1-UTC (SEC)
//     DXINT - INTERPOLATED VALUE OF dX CELESTIAL POLE OFFSET (MAS)
//     DYINT - INTERPOLATED VALUE OF dY CELESTIAL POLE OFFSET (MAS)

	double cor_x, cor_y, cor_ut1, cor_lod;
	double cor_dX, cor_dY;
	double ARG[6], DARG[6];

	if ( EOPs == 0 ) {
		x_int   = LAGINT( MJD, X,  N, mjd_int );
		y_int   = LAGINT( MJD, Y,  N, mjd_int );
		ut1_int = LAGINT( MJD, T,  N, mjd_int );
		dX_int  = LAGINT( MJD, DX, N, mjd_int );
		dY_int  = LAGINT( MJD, DY, N, mjd_int );
	}
	else {

		double d_mjd = mjd_int - EOPs[0];

		if ( EOPs[1] == 0.0 )
			x_int   = LAGINT( MJD, X,  N, mjd_int );
		else
			x_int   = EOPs[1] + d_mjd * EOPs[4];

		if ( EOPs[2] == 0.0 )
			y_int   = LAGINT( MJD, Y,  N, mjd_int );
		else
			y_int   = EOPs[2] + d_mjd * EOPs[5];

		if ( EOPs[3] == 0.0 )
			ut1_int = LAGINT( MJD, T,  N, mjd_int );
		else
			ut1_int = EOPs[3] - d_mjd * EOPs[6];

		//dX_int  = LAGINT( MJD, DX, N, mjd_int );
		//dY_int  = LAGINT( MJD, DY, N, mjd_int );

		// Free-core nutation
		FCNNUT( mjd_int, cor_dX, cor_dY );

		dX_int = cor_dX;
		dY_int = cor_dY;
	}

	// Calculate fundamental arguments
	FUNDARG( mjd_int, ARG, DARG );

	// Oceanic effect      
	PMUT1_OCEANS( mjd_int, ARG, DARG, cor_x, cor_y, cor_ut1, cor_lod );

	x_int += cor_x;
	y_int += cor_y;
	ut1_int += cor_ut1;

	// Lunisolar effect (libration)
	PM_GRAVI( mjd_int, ARG, cor_x, cor_y );

	x_int += cor_x;
	y_int += cor_y;

	// UT1 libration
	UTLIBR( mjd_int, ARG, cor_ut1, cor_lod ); 
	ut1_int += cor_ut1;
}

//
//----------------------------------------------------------------
//
double LAGINT( double X[], double Y[], int N,  double XINT )
{
// 
//     THIS SUBROUTINE PERFORMS LAGRANGIAN INTERPOLATION
//     WITHIN A SET OF (X,Y) PAIRS TO GIVE THE Y
//     VALUE CORRESPONDING TO XINT.  THIS PROGRAM USES A
//     WINDOW OF 4 DATA POINTS TO PERFORM THE INTERPOLATION.
//     IF THE WINDOW SIZE NEEDS TO BE CHANGED, THIS CAN BE
//     DONE BY CHANGING THE INDICES IN THE DO LOOPS FOR
//     VARIABLES M AND J.
//
//     PARAMETERS ARE :
//     X     - ARRAY OF VALUES OF THE INDEPENDENT VARIABLE
//     Y     - ARRAY OF FUNCTION VALUES CORRESPONDING TO X
//     N     - NUMBER OF POINTS
//     XINT  - THE X-VALUE FOR WHICH ESTIMATE OF Y IS DESIRED
//     YOUT  - THE Y VALUE RETURNED TO CALLER

	double YOUT, TERM;
	int I, J, K, M;

	YOUT = 0.0;
	for ( I = 1; I < N; ++I ) {
		if ( XINT >= X[I] && XINT < X[I+1] )
			K = I;
	}

	if ( K < 1 ) K = 1;
	if ( K > N-3 ) K = N-3;

	for ( M = K-1; M < K+3; ++M ) {
		TERM = Y[M];

		for ( J = K-1; J < K+3; ++J ) {
			if ( M != J )
				TERM *= (XINT - X[J])/(X[M] - X[J]);
		}

		YOUT += TERM;
	}

	return YOUT;
}

void FUNDARG( double rjd, double ARG[6], double DARG[6] )
{
	const double halfpi = 1.5707963267948966;
	const double secrad = 2.0 * halfpi / (180.0*3600.0);

	double T, T2, T3, T4;

	T = (rjd - 51544.5)/36525.0;  // julian century
	T2 = T*T;
	T3 = T2*T;
	T4 = T2*T2;

	// Arguments in the following order : chi=GMST+pi,l,lp,F,D,Omega
	// et leur derivee temporelle 

	ARG[0] = (67310.54841 + (876600.0*3600.0 + 8640184.812866)*T + 0.093104*T2 - 6.2e-6*T3)*15.0 + 648000.0;
	ARG[0] = fmod( ARG[0], 1296000.0 ) * secrad;

	DARG[0] = (876600.0*3600.0 + 8640184.812866 + 2.0 * 0.093104 * T - 3.0 * 6.2e-6*T2) * 15.0;
	DARG[0] *= secrad / 36525.0;  // rad/day

	ARG[1] = -0.00024470*T4 + 0.051635*T3 + 31.8792*T2 + 1717915923.2178*T + 485868.249036;
	ARG[1] = fmod(ARG[1],1296000.0)*secrad;

	DARG[1] = -4.0*0.00024470*T3 + 3.0*0.051635*T2 + 2.0*31.8792*T + 1717915923.2178; 
	DARG[1] *= secrad / 36525.0;   // rad/day

	ARG[2] = -0.00001149*T4 + 0.000136*T3 - 0.5532*T2 + 129596581.0481*T + 1287104.79305;
	ARG[2] = fmod(ARG[2],1296000.0)*secrad;

	DARG[2] = -4.0*0.00001149*T3 - 3.0*0.000136*T2 - 2.0*0.5532*T + 129596581.0481;
	DARG[2] *= secrad / 36525.0;   // rad/day

	ARG[3] = 0.00000417*T4 - 0.001037*T3 - 12.7512*T2 + 1739527262.8478*T + 335779.526232;
	ARG[3] = fmod( ARG[3],1296000.0)*secrad;

	DARG[3] = 4.0*0.00000417*T3 - 3.0*0.001037*T2 - 2.0 * 12.7512*T + 1739527262.8478; 
	DARG[3] *= secrad / 36525.0;   // rad/day

	ARG[4] = -0.00003169*T4 + 0.006593*T3 - 6.3706*T2 + 1602961601.2090*T + 1072260.70369;
	ARG[4] = fmod( ARG[4],1296000.0)*secrad;

	DARG[4] = -4.0*0.00003169*T3 + 3.0*0.006593*T2 - 2.0 * 6.3706*T + 1602961601.2090;
	DARG[4] *= secrad / 36525.0;   // rad/day

	ARG[5] = -0.00005939*T4 + 0.007702*T3 + 7.4722*T2 - 6962890.5431*T + 450160.398036;
	ARG[5] = fmod( ARG[5], 1296000.0) * secrad;

	DARG[5] = -4.0*0.00005939*T3 + 3.0 * 0.007702*T2 + 2.0 * 7.4722*T - 6962890.2665;
	DARG[5] *= secrad / 36525.0;   // rad/day
}

//----------------------------------------------------------------
void PMUT1_OCEANS( double rjd, const double ARG[6], const double DARG[6], double &cor_x, double &cor_y, double &cor_ut1, double &cor_lod )
{
//
//    This subroutine provides, in time domain, the diurnal/subdiurnal
//    tidal effets on polar motion ("), UT1 (s) and LOD (s). The tidal terms,
//    listed in the program above, have been extracted from the procedure   
//    ortho_eop.f coed by Eanes in 1997.
//    
//    N.B.:  The fundamental lunisolar arguments are those of Simon et al.  
//
//    These corrections should be added to "average"
//    EOP values to get estimates of the instantaneous values.
//
//     PARAMETERS ARE :
//     rjd      - epoch of interest given in mjd
//     cor_x    - tidal correction in x (sec. of arc)
//     cor_y    - tidal correction in y (sec. of arc)
//     cor_ut1  - tidal correction in UT1-UTC (sec. of time)
//     cor_lod  - tidal correction in length of day (sec. of time)
//
//     coded by Ch. Bizouard (2002), initially coded by McCarthy and 
//     D.Gambis(1997) for the 8 prominent tidal waves.  

	const double halfpi = 1.5707963267948966;
	const double secrad = 2.0 * halfpi / (180.0*3600.0);

	const int nlines = 71;


	double ag, dag;
	int i, j;

	//  Oceanic tidal terms present in x (microas),y(microas),ut1(microseconds)       
	//  NARG(j,6) : Multipliers of GMST+pi and Delaunay arguments. 

	const double XSIN[]  = {-0.05, 0.06, 0.30, 0.08, 0.46, 1.19, 6.24, 0.24, 1.28,-0.28, 9.22,48.82,-0.32,-0.66,-0.42,-0.30,-1.61,-4.48,-0.90,-0.86, 1.54,-0.29,26.13,-0.22,-0.61, 1.54,-77.48,-10.52, 0.23,-0.61,-1.09,-0.69,-3.46,-0.69,-0.37,-0.17,-1.10,-0.70,-0.15,-0.03,-0.02,-0.49,-1.33,-6.08,-7.59,-0.52, 0.47, 2.12,-56.87,-0.54,-11.01,-0.51, 0.98, 1.13,12.32,-330.15,-1.01, 2.47, 9.40,-2.35,-1.04,-8.51,-144.13, 1.19, 0.49,-38.48,-11.44,-1.24,-1.77,-0.77,-0.33 };
	const double XCOS[]  = { 0.94, 0.64, 3.42, 0.78, 4.15, 4.96,26.31, 0.94, 4.99,-0.77,25.06,132.91,-0.86,-1.72,-0.92,-0.64,-3.46,-9.61,-1.93,-1.81, 3.03,-0.58,51.25,-0.42,-1.20, 3.00,-151.74,-20.56, 0.44,-1.19,-2.11,-1.43,-7.28,-1.44,-1.06,-0.51,-3.42,-2.19,-0.46,-0.59,-0.38,-0.04,-0.17,-1.61,-2.05,-0.14, 0.11, 0.49,-12.93,-0.12,-2.40,-0.11, 0.11, 0.11, 1.00,-26.96,-0.07,-0.28,-1.44, 0.37, 0.17, 3.50,63.56,-0.56,-0.25,19.14, 5.75, 0.63, 1.79, 0.78, 0.62 };
	const double YSIN[]  = {-0.94,-0.64,-3.42,-0.78,-4.15,-4.96,-26.31,-0.94,-4.99, 0.77,-25.06,-132.90, 0.86, 1.72, 0.92, 0.64, 3.46, 9.61, 1.93, 1.81,-3.03, 0.58,-51.25, 0.42, 1.20,-3.00,151.74,20.56,-0.44, 1.19, 2.11, 1.43, 7.28, 1.44, 1.06, 0.51, 3.42, 2.19, 0.46, 0.59, 0.38, 0.63, 1.53, 3.13, 3.44, 0.22,-0.10,-0.41,11.15, 0.10, 1.89, 0.08,-0.11,-0.13,-1.41,37.58, 0.11,-0.44,-1.88, 0.47, 0.21, 3.29,59.23,-0.52,-0.23,17.72, 5.32, 0.58, 1.71, 0.75, 0.65 };
	const double YCOS[]  = {-0.05, 0.06, 0.30, 0.08, 0.45, 1.19, 6.23, 0.24, 1.28,-0.28, 9.22,48.82,-0.32,-0.66,-0.42,-0.30,-1.61,-4.48,-0.90,-0.86, 1.54,-0.29,26.13,-0.22,-0.61, 1.54,-77.48,-10.52, 0.23,-0.61,-1.09,-0.69,-3.46,-0.69,-0.37,-0.17,-1.09,-0.70,-0.15,-0.03,-0.02, 0.24, 0.68, 3.35, 4.23, 0.29,-0.27,-1.23,32.88, 0.31, 6.41, 0.30,-0.58,-0.67,-7.31,195.92, 0.60,-1.48,-5.65, 1.41, 0.62, 5.11,86.56,-0.72,-0.29,23.11, 6.87, 0.75, 1.04, 0.45, 0.19 };
	const double UTSIN[] = { 0.396,0.195,1.034,0.224,1.187,0.966,5.118,0.172,0.911,-0.093,3.025,16.020,-0.103,-0.194,-0.083,-0.057,-0.308,-0.856,-0.172,-0.161,0.315,-0.062,5.512,-0.047,-0.134,0.348,-17.620,-2.392,0.052,-0.144,-0.267,-0.288,-1.610,-0.320,-0.407,-0.213,-1.436,-0.921,-0.193,-0.396,-0.253,-0.089,-0.224,-0.637,-0.745,-0.049,0.033,0.141,-3.795,-0.035,-0.698,-0.032,0.050,0.056,0.605,-16.195,-0.049,0.111,0.425,-0.106,-0.047,-0.437,-7.547,0.064,0.027,-2.104,-0.627,-0.068,-0.146,-0.064,-0.049 };
	const double UTCOS[] = {-0.078,-0.059,-0.314,-0.073,-0.387,-0.474,-2.499,-0.090,-0.475,0.070,-2.280,-12.069,0.078,0.154,0.074,0.050,0.271,0.751,0.151,0.137,-0.189,0.035,-3.095,0.025,0.070,-0.171,8.548,1.159,-0.025,0.065,0.111,0.043,0.187,0.037,-0.005,-0.005,-0.037,-0.023,-0.005,-0.024,-0.015,-0.011,-0.032,-0.177,-0.222,-0.015,0.013,0.058,-1.556,-0.015,-0.298,-0.014,0.022,0.025,0.266,-7.140,-0.021,0.034,0.117,-0.029,-0.013,-0.019,-0.159,0.000,-0.001,0.041,0.015,0.002,0.037,0.017,0.018 };

	const int  NARG[][6] = {
		1, -1,  0, -2, -2, -2,
		1, -2,  0, -2,  0, -1,
		1, -2,  0, -2,  0, -2,
		1,  0,  0, -2, -2, -1,
		1,  0,  0, -2, -2, -2,
		1, -1,  0, -2,  0, -1,
		1, -1,  0, -2,  0, -2,
		1,  1,  0, -2, -2, -1,
		1,  1,  0, -2, -2, -2,
		1,  0,  0, -2,  0,  0,
		1,  0,  0, -2,  0, -1,
		1,  0,  0, -2,  0, -2,
		1, -2,  0,  0,  0,  0,
		1,  0,  0,  0, -2,  0,
		1, -1,  0, -2,  2, -2,
		1,  1,  0, -2,  0, -1,
		1,  1,  0, -2,  0, -2,
		1, -1,  0,  0,  0,  0,
		1, -1,  0,  0,  0, -1,
		1,  1,  0,  0, -2,  0,
		1,  0, -1, -2,  2, -2,
		1,  0,  0, -2,  2, -1,
		1,  0,  0, -2,  2, -2,
		1,  0,  1, -2,  2, -2,
		1,  0, -1,  0,  0,  0,
		1,  0,  0,  0,  0,  1,
		1,  0,  0,  0,  0,  0,
		1,  0,  0,  0,  0, -1,
		1,  0,  0,  0,  0, -2,
		1,  0,  1,  0,  0,  0,
		1,  0,  0,  2, -2,  2,
		1, -1,  0,  0,  2,  0,
		1,  1,  0,  0,  0,  0,
		1,  1,  0,  0,  0, -1,
		1,  0,  0,  0,  2,  0,
		1,  2,  0,  0,  0,  0,
		1,  0,  0,  2,  0,  2,
		1,  0,  0,  2,  0,  1,
		1,  0,  0,  2,  0,  0,
		1,  1,  0,  2,  0,  2,
		1,  1,  0,  2,  0,  1,
		2, -3,  0, -2,  0, -2,
		2, -1,  0, -2, -2, -2,
		2, -2,  0, -2,  0, -2,
		2,  0,  0, -2, -2, -2,
		2,  0,  1, -2, -2, -2,
		2, -1, -1, -2,  0, -2,
		2, -1,  0, -2,  0, -1,
		2, -1,  0, -2,  0, -2,
		2, -1,  1, -2,  0, -2,
		2,  1,  0, -2, -2, -2,
		2,  1,  1, -2, -2, -2,
		2, -2,  0, -2,  2, -2,
		2,  0, -1, -2,  0, -2,
		2,  0,  0, -2,  0, -1,
		2,  0,  0, -2,  0, -2,
		2,  0,  1, -2,  0, -2,
		2, -1,  0, -2,  2, -2,
		2,  1,  0, -2,  0, -2,
		2, -1,  0,  0,  0,  0,
		2, -1,  0,  0,  0, -1,
		2,  0, -1, -2,  2, -2,
		2,  0,  0, -2,  2, -2,
		2,  0,  1, -2,  2, -2,
		2,  0,  0,  0,  0,  1,
		2,  0,  0,  0,  0,  0,
		2,  0,  0,  0,  0, -1,
		2,  0,  0,  0,  0, -2,
		2,  1,  0,  0,  0,  0,
		2,  1,  0,  0,  0, -1,
		2,  0,  0,  2,  0,  2,

	};



	// CORRECTIONS

	cor_x   = 0.0;
	cor_y   = 0.0;
	cor_ut1 = 0.0;
	cor_lod = 0.0;

	for ( j = 0; j < nlines; ++j ) {

		ag  = 0.0;
		dag = 0.0;

		for ( i = 0; i < 6; ++i ) {
			ag  += double(NARG[j][i]) *  ARG[i];
			dag += double(NARG[j][i]) * DARG[i];
		}

		ag = fmod( ag, 4.0*halfpi );

		cor_x   +=    XCOS[j] * cos(ag) +  XSIN[j] * sin(ag);
		cor_y   +=    YCOS[j] * cos(ag) +  YSIN[j] * sin(ag);
		cor_ut1 +=   UTCOS[j] * cos(ag) + UTSIN[j] * sin(ag);
		cor_lod -= (-UTCOS[j] * sin(ag) + UTSIN[j] * cos(ag) ) * dag;

	}

	cor_x   *= 1.0e-6;  // uas -> arcsecond (")
	cor_y   *= 1.0e-6;  // uas -> arcsecond (")
	cor_ut1 *= 1.0e-6;  // us -> second (s)
	cor_lod *= 1.0e-6;  // us -> second (s)

}
      	
//----------------------------------------------------------------
void PM_GRAVI( double rjd, const double ARG[6], double &cor_x, double &cor_y )
{
//
//    This subroutine provides, in time domain, the diurnal
//    lunisolar effet on polar motion (")
//    
//    N.B.:  The fundamental lunisolar arguments are those of Simon et al.  
//
//    These corrections should be added to "average"
//    EOP values to get estimates of the instantaneous values.
//
//     PARAMETERS ARE :
//     rjd      - epoch of interest given in mjd
//     cor_x    - tidal correction in x (sec. of arc)
//     cor_y    - tidal correction in y (sec. of arc)
//
//     coded by Ch. Bizouard (2002)

	const double halfpi = 1.5707963267948966;
	const double secrad = 2.0 *halfpi / (180.0*3600.0);

	const int nlines = 10;

	double ag;
	int i, j;

	//  Diurnal lunisolar tidal terms present in x (microas),y(microas)      
	//  NARG(j,6) : Multipliers of GMST+pi and Delaunay arguments. 

	const double XSIN[] = {-0.44,-2.31,-0.44,-2.14,-11.36, 0.84,-4.76,14.27, 1.93, 0.76 };
	const double XCOS[] = { 0.25, 1.32, 0.25, 1.23, 6.52,-0.48, 2.73,-8.19,-1.11,-0.43 };
	const double YSIN[] = {-0.25,-1.32,-0.25,-1.23,-6.52, 0.48,-2.73, 8.19, 1.11, 0.43 };
	const double YCOS[] = {-0.44,-2.31,-0.44,-2.14,-11.36, 0.84,-4.76,14.27, 1.93, 0.76 };

	const int NARG[][6] = {
	  1, -1,  0, -2,  0, -1,
	  1, -1,  0, -2,  0, -2,
	  1,  1,  0, -2, -2, -2,
	  1,  0,  0, -2,  0, -1,
	  1,  0,  0, -2,  0, -2,
	  1, -1,  0,  0,  0,  0,
	  1,  0,  0, -2,  2, -2,
	  1,  0,  0,  0,  0,  0,
	  1,  0,  0,  0,  0, -1,
	  1,  1,  0,  0,  0,  0
	};

	// CORRECTIONS

	cor_x  = 0.0;
	cor_y  = 0.0;

	for ( j = 0; j < nlines; ++j ) {

		ag  = 0.0;
		for ( i = 0; i < 6; ++i ) {
			ag += double(NARG[j][i]) * ARG[i];
		}
		ag = fmod( ag, 4.0*halfpi );

		cor_x += XCOS[j] * cos(ag) + XSIN[j] * sin(ag);
		cor_y += YCOS[j] * cos(ag) + YSIN[j] * sin(ag);

	}

	cor_x *= 1.0e-6;   // uas -> arcsecond (")
	cor_y *= 1.0e-6;   // uas -> arcsecond (")
 
}

void UTLIBR( double RMJD, const double ARG[6], double &cor_ut1, double &cor_lod )
{
	//  rmjd0   - modified Julian date of J2000
	const double RMJD0   = 51544.5;
	const double TWOPI   = 6.283185307179586476925287;

	const int nlines = 11;

	int i, j;
	double ag;

	// Coefficients of the quasi semidiurnal terms in dUT1, dLOD 
	// Source: IERS Conventions (2010), Table 5.1b

	const double UT1SIN[] = { 0.05, 0.06, 0.35, 0.07,-0.07, 1.75,-0.05, 0.04, 0.76, 0.21, 0.06 };
	const double UT1COS[] = {-0.03,-0.03,-0.20,-0.04, 0.04,-1.01, 0.03,-0.03,-0.44,-0.12,-0.04 };
	const double LODSIN[] = {-0.3, -0.4, -2.4, -0.5,  0.5,-12.2,  0.3, -0.3, -5.5, -1.5, -0.4 };
	const double LODCOS[] = {-0.6, -0.7, -4.1, -0.8,  0.8,-21.3,  0.6, -0.6, -9.6, -2.6, -0.8 };

	const int NARG[][6] = {
		2, -2,  0, -2,  0, -2,
		2,  0,  0, -2, -2, -2,
		2, -1,  0, -2,  0, -2,
		2,  1,  0, -2, -2, -2,
		2,  0,  0, -2,  0, -1,
		2,  0,  0, -2,  0, -2,
		2,  1,  0, -2,  0, -2,
		2,  0, -1, -2,  2, -2,
		2,  0,  0, -2,  2, -2,
		2,  0,  0,  0,  0,  0,
		2,  0,  0,  0,  0, -1
	};


	// Compute the harmonic model of dUT1 and dLOD 
	// dUT1 and dLOD are set to zero first 

	cor_ut1 = 0.0;
	cor_lod = 0.0;

	for ( j = 0; j < nlines; ++j ) {

		// For the j-th term of the trigonometric expansion, compute the angular
		// argument angle of sine and cosine functions as a linear integer
		// combination of the 6 fundamental arguments
		ag  = 0.0;
		for ( i = 0; i < 6; ++i ) {
			ag += double( NARG[j][i] ) * ARG[i];
		}
		ag = fmod( ag, TWOPI );

		// Compute contribution from the j-th term of expansion to dUT1 and dLOD 
		cor_ut1 += UT1SIN[j] * sin(ag) + UT1COS[j] * cos(ag);
		cor_lod += LODSIN[j] * sin(ag) + LODCOS[j] * cos(ag);
	}

	cor_ut1 *= 1.0e-6;   // usec -> sec
	cor_lod *= 1.0e-6;   // usec -> sec
}

void FCNNUT( double MJD, double &dX, double &dY )
{
//
//  - - - - - - - - - - -
//   F C N N U T
//  - - - - - - - - - - -
//
//  This routine is part of the International Earth Rotation and
//  Reference Systems Service (IERS) Conventions software collection.
//
//  This subroutine computes the effects of the free core nutation.  
//  Please note that the table is updated each year (see Note 4).
//  The parameter N needs to be incremented for each additional
//  year in the table.
//
//  In general, Class 1, 2, and 3 models represent physical effects that
//  act on geodetic parameters while canonical models provide lower-level
//  representations or basic computations that are used by Class 1, 2, or
//  3 models.
// 
//  Status: Class 3 model
//
//     Class 1 models are those recommended to be used a priori in the
//     reduction of raw space geodetic data in order to determine
//     geodetic parameter estimates.
//     Class 2 models are those that eliminate an observational
//     singularity and are purely conventional in nature.
//     Class 3 models are those that are not required as either Class
//     1 or 2.
//     Canonical models are accepted as is and cannot be classified as a
//     Class 1, 2, or 3 model.
//
//  Given:
//     mjd           d      Modified Julian Date, TDB (Note 1)
//
//  Returned:
//     dX             d      CIP offset x component, in milias (Note 2)
//     dY             d      CIP offset y component, in milias (Note 2)
//
//  Notes:
//
//  1) Though the Modified Julian Date (MJD) is strictly TDB, it is
//     usually more convenient to use TT, which makes no significant
//     difference.
//
//  1) CIP is the Celestial Intermediate Pole.  The expression
//     used is given in microarcseconds.
//  
//  2) The expression used is given in microarcseconds.
//
//  3) The updated table is maintained at the website
//     http://syrte.obspm.fr/~lambert/fcn/.
//
//  Test case:
//     given input: MJD = 54790   Modified Julian Date, TDB
//                  
//     expected output:  X = -178.5138569053294191 microarcseconds
//                       Y = -96.32065993534820336 microarcseconds
//                       dX = 8.940983606557377783 microarcseconds
//                       dY = 8.940983606557377783 microarcseconds
//
//  References:
//
//     Petit, G. and Luzum, B. (eds.), IERS Conventions (2010),
//     IERS Technical Note No. 36, BKG (2010)
//
//  Revisions:
//  2007 August   27 S. Lambert    Original code
//  2008 November 19 B.E.Stetzler  Added header and copyright
//  2008 November 20 B.E.Stetzler  provided a test case 
//  2009 July     10 B.E.Stetzler  Updated parameter N to 26, 
//                                 updated table to 2009, and corrected
//                                 test case results
//  2009 August   18 B.E.Stetzler  Capitalized all variables for FORTRAN
//                                 77 compatibility
//  2010 July      2 B.E.Stetzler  Updated parameter N to 27, 
//                                 updated table to 2010, and corrected
//                                 test case results
//-----------------------------------------------------------------------

	const double PI = 3.14159265358979323846;

	double TABLE[][4] = {
		45700.0,  -219.93,  -155.91,     8.32, // 1984.0
		46066.0,  -245.97,  -149.26,     6.76, // 1985.0
		46431.0,  -247.42,   -97.46,     5.75, // 1986.0
		46796.0,  -232.93,  -118.49,     4.95, // 1987.0
		47161.0,  -218.13,   -76.40,     4.27, // 1988.0
		47527.0,  -200.95,   -43.62,     3.71, // 1989.0
		47892.0,  -184.49,   -14.01,     3.32, // 1990.0
		48257.0,  -171.29,    -0.10,     3.34, // 1991.0
		48622.0,  -158.71,    13.29,     3.37, // 1992.0
		48988.0,  -142.91,     7.30,     3.36, // 1993.0
		49353.0,  -135.67,    27.63,     3.39, // 1994.0
		49718.0,  -113.62,    32.95,     2.94, // 1995.0
		50083.0,   -89.39,    29.53,     2.68, // 1996.0
		50449.0,   -67.61,     4.93,     2.53, // 1997.0
		50814.0,   -39.00,   -21.69,     2.21, // 1998.0
		51179.0,    -2.66,   -56.95,     1.93, // 1999.0
		51544.0,    16.85,   -66.32,     1.76, // 2000.0
		51910.0,    44.47,   -54.65,     1.58, // 2001.0
		52275.0,    64.48,   -56.76,     1.52, // 2002.0
		52640.0,    88.76,   -57.87,     1.49, // 2003.0
		53005.0,   113.38,   -30.49,     1.34, // 2004.0
		53371.0,   134.47,    -0.56,     1.24, // 2005.0
		53736.0,   145.18,    49.72,     1.11, // 2006.0
		54101.0,   144.64,    73.19,     1.10, // 2007.0
		54466.0,   120.61,   106.58,     1.04, // 2008.0
		54832.0,    72.15,   182.43,     0.84, // 2009.0
		55197.0,    76.37,   208.46,     0.78, // 2010.0
		55562.0,    79.31,   217.89,     0.78, // 2011.0
		55927.0,    80.66,   220.96,     0.76, // 2012.0
		56293.0,    63.49,   227.45,     0.80, // 2013.0
		56658.0,    61.63,   232.20,     0.82, // 2014.0
		57023.0,    69.78,   235.49,     0.88, // 2015.0
	};

	const int N = sizeof(TABLE) / 4 / sizeof(double);

	//       Internal variables

	int I, J;
	double PER,PHI,MPE;
	double AXC,AXS,AYC,AYS;
	double DAXC,DAXS,DAYC,DAYS,DT,T;
	double DATE[N],XC[N],XS[N],YC[N],YS[N],SX[N],SY[N];


	//       Mean prediction error

	MPE = 0.1325;                        // microarcseconds per day

	//       FCN parameters

	PER = -430.21;                       // period in days
	PHI = (2.0*PI/PER)*(MJD-51544.5);    // phase in radians

	//       Block data of amplitudes for X (microas)


	//       Amplitudes extracted from the table

	for ( I = 0; I < N; ++I ) {
		DATE[I]=TABLE[I][0];
		XC[I]=TABLE[I][1];
		XS[I]=TABLE[I][2];
		SX[I]=TABLE[I][3];
		YC[I]=XS[I];
		YS[I]=-XC[I];
		SY[I]=SX[I];
	}

	//       Prediction of the amplitude at the input date

	if ( MJD <= DATE[0] ) {
		AXC=XC[0];
		AXS=XS[0];
		AYC=YC[0];
		AYS=YS[0];
	}
	else if ( MJD >= DATE[N-1] ) {
		AXC=XC[N-1];
		AXS=XS[N-1];
		AYC=YC[N-1];
		AYS=YS[N-1];
	}
	else {
		for ( I = 0; I < N-1; ++I ) {
			if ( MJD >= DATE[I] && MJD < DATE[I+1] ) {
				T=MJD-DATE[I];
				DT=DATE[I+1]-DATE[I];
				DAXC=XC[I+1]-XC[I];
				DAXS=XS[I+1]-XS[I];
				DAYC=YC[I+1]-YC[I];
				DAYS=YS[I+1]-YS[I];
				AXC=XC[I]+(DAXC/DT)*T;
				AXS=XS[I]+(DAXS/DT)*T;
				AYC=YC[I]+(DAYC/DT)*T;
				AYS=YS[I]+(DAYS/DT)*T;
			}
		}
	}

	//       Computation of X and Y

	dX=AXC*cos(PHI)-AXS*sin(PHI);  // uas
	dY=AYC*cos(PHI)-AYS*sin(PHI);  // uas

	dX *= 1e-3;                    // uas -> mas
	dY *= 1e-3;                    // uas -> mas
}