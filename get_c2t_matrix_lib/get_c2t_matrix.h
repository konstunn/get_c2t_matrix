struct EOP_PARTIALS {
	double xpole_p[3][3];
	double xpole_rate_p[3][3];
	double ypole_p[3][3];
	double ypole_rate_p[3][3];
	double ut1_rate_p[3][3];
};


int get_c2t_matrix( double UTC, double c2t_matrix[3][3], double pole[2], double *EOPs, EOP_PARTIALS *part );
