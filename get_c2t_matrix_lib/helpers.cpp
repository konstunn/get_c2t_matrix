#define _USE_MATH_DEFINES
#include <cmath>
#include <cstring>
#include <cctype>


int myround( double number )
{
    return (number >= 0) ? (int) (number + 0.5) : (int) (number - 0.5);
}

double mod( double x, double y )
{
	int ch = (int) ( x / y );

	return x - ( ch * y );
}

double katan2( double y, double x )
{
	if ( x > 0.0 )
		return atan( y / x );
	else if ( x == 0.0 )
		return ( y >= 0.0 ) ? M_PI / 2 : -M_PI / 2;
	else 
		return ( y >= 0.0 ) ? atan( y / x ) + M_PI : atan( y / x ) - M_PI;
}

const char* katoi( const char *str, int *number, unsigned len )
{
	bool minus = false;
	int res = 0;

	while ( isspace( *str ) )
		++str;

	if ( *str == '-' ) {
		minus = true;
		++str;
	}
	if ( *str == '+' )
		++str;

	while ( *str && isdigit( *str ) && len ) {
		int dig = *str - '0';
		res = res * 10 + dig;

		--len;
		++str;
	}

	*number = (minus) ? -res : res;

	return str;
}

const char* katof( const char *str, double *number, unsigned totLen, unsigned fracLen )
{
	int sign = 1;
	double res = 0.0;
	double dividor = 1.0;

	while ( totLen && isspace( *str ) )
		++str, --totLen;

	if ( totLen == 0 )
		goto exit;

	if ( *str == '-' ) {
		sign = -1;
		++str, --totLen;
	}
	if ( *str == '+' )
		++str, --totLen;

	while ( *str && isdigit( *str ) && totLen ) {
		int dig = *str - '0';
		res = res * 10 + dig;

		--totLen;
		++str;
	}

	if ( *str == '.' || *str == ',' )
		++str, --totLen;

	while ( *str && isdigit( *str ) && totLen && fracLen ) {
		int dig = *str - '0';
		res = res * 10 + dig;
		dividor *= 10;

		--totLen;
		--fracLen;
		++str;
	}

	if ( *str == 'E' || *str == 'e' || *str == 'D' || *str == 'd' ) {
		++str, --totLen;

		int e;
		double dividor2;

		str = katoi( str, &e, totLen );

		dividor /= pow( 10.0, e );
	}

exit:

	*number = sign * res / dividor;

	return str;
}

