#pragma once

int myround( double number );
double mod( double x, double y );
const char* katoi( const char *str, int *number, unsigned len );
const char* katof( const char *str, double *number, unsigned totLen, unsigned fracLen );
