#include "mex.h"
#include "sofa.h"
#include <time.h>

extern "C" void mexFunction( int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

const double mjd_st = 57286.0;
const double EOPs[] = { 57286.5, 0.219151, 0.335101,  0.2461278, -682e-6, -2071e-6 };

void main( void )
{
	double frac = 22500.0 / 86400.0;

	double gmst = iauGmst06( 2400000.5+mjd_st, frac, 2400000.5+mjd_st, frac );

	const mxArray *input[3];
	mxArray *output[6];
	double *ptr;
	double *inp_EOPs;
	double *c2tm;
	double t = 0.0;

	input[0] = mxCreateDoubleMatrix( 1, 1, mxREAL );
	ptr = mxGetPr( input[0] );

	input[1] = mxCreateDoubleMatrix( 1, 6, mxREAL );
	inp_EOPs = mxGetPr( input[1] );
	inp_EOPs[0] = EOPs[0];
	inp_EOPs[1] = EOPs[1];
	inp_EOPs[2] = EOPs[2];
	inp_EOPs[3] = EOPs[3];
	inp_EOPs[4] = EOPs[4];
	inp_EOPs[5] = EOPs[5];

	clock_t clk = clock();

	for ( t = 0.0; t < 86400.0; t += 3.0 ) {
		*ptr = mjd_st + t / 86400.0;

		mexFunction( 6, output, 2, input );

		c2tm = mxGetPr( output[0] );

		printf( "%16.16f\t%16.16f\t%16.16f\n%16.16f\t%16.16f\t%16.16f\n%16.16f\t%16.16f\t%16.16f\n", c2tm[0], c2tm[1], c2tm[2], c2tm[3], c2tm[4], c2tm[5], c2tm[6], c2tm[7], c2tm[9] );
	}


	clk = clock() - clk;
	c2tm = mxGetPr( output[0] );

	printf( "%16.16f\t%16.16f\t%16.16f\n", c2tm[0], c2tm[1], c2tm[2] );
	printf ("It took me %d clicks (%f seconds).\n",clk,((double)clk)/CLOCKS_PER_SEC);

	getchar();
}