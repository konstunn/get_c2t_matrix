#include "mex.h"
#include "..\get_c2t_matrix_lib\get_c2t_matrix.h"


/* The gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double UTC;              /* input time */
    double *outMatrix;       /* output matrix */		
	double *EOPs = 0;        /* external EOPs */
	double pole[2];          /* X and Y of pole */
	double C2T[3][3];        /* celestial to terrestial matrix */
	EOP_PARTIALS part;       /* EOP Partials */
	EOP_PARTIALS *part_ptr = &part;

    /* check for proper number of arguments */
    if( nrhs != 1 && nrhs != 2 ) {
        mexErrMsgIdAndTxt("MyToolbox:get_c2t_matrix:nrhs","One of two inputs required.");
    }
    if( nlhs != 1 && nlhs != 3 && nlhs != 6 ) {
        mexErrMsgIdAndTxt("MyToolbox:get_c2t_matrix:nlhs","One, three or six outputs required.");
    }

    /* make sure the first input argument is scalar */
    if( !mxIsDouble(prhs[0]) || mxGetNumberOfElements(prhs[0])!=1 ) {
        mexErrMsgIdAndTxt("MyToolbox:get_c2t_matrix:notScalar","First input must be a double scalar.");
    }
    /* make sure the second input argument is double vector */
    if( nrhs > 1 && !mxIsDouble(prhs[1]) ) {
        mexErrMsgIdAndTxt("MyToolbox:get_c2t_matrix:notScalar","Second input must be a double.");
    }

	/* EOP partials are not needed */
	if ( nlhs != 6 )
		part_ptr = 0;

    /* get the value of the scalar input  */
    UTC = mxGetScalar(prhs[0]);

    /* create the output matrix */
    plhs[0] = mxCreateDoubleMatrix(3,3,mxREAL);

    /* get a pointer to the real data in the output matrix */
    outMatrix = mxGetPr(plhs[0]);
	if ( nrhs > 1 && mxGetNumberOfElements(prhs[1]) == 7 )
		EOPs = mxGetPr( prhs[1] );

    /* call the computational routine */

    int res = get_c2t_matrix( UTC, C2T, pole, EOPs, part_ptr );

	for ( int i = 0; i < 3; ++i ) {
		for ( int j = 0; j < 3; ++j )
			outMatrix[i + 3*j] = C2T[i][j];
	}
	
	if ( res == 0 )
		mexErrMsgIdAndTxt("MyToolbox:get_c2t_matrix:notFoundEOPs","EOPs was not found.");

	if ( nlhs == 3 ) {
		plhs[1] = mxCreateDoubleScalar( pole[0] );
		plhs[2] = mxCreateDoubleScalar( pole[1] );
	}

	if ( nlhs == 6 ) {
		plhs[1] = mxCreateDoubleMatrix( 3, 3, mxREAL );
		plhs[2] = mxCreateDoubleMatrix( 3, 3, mxREAL );
		plhs[3] = mxCreateDoubleMatrix( 3, 3, mxREAL );
		plhs[4] = mxCreateDoubleMatrix( 3, 3, mxREAL );
		plhs[5] = mxCreateDoubleMatrix( 3, 3, mxREAL );

		double *p1 = mxGetPr( plhs[1] );
		double *p2 = mxGetPr( plhs[2] );
		double *p3 = mxGetPr( plhs[3] );
		double *p4 = mxGetPr( plhs[4] );
		double *p5 = mxGetPr( plhs[5] );

		for ( int i = 0; i < 3; ++i ) {
			for ( int j = 0; j < 3; ++j ) {
				p1[i + 3*j] = part.xpole_p[i][j];
				p2[i + 3*j] = part.xpole_rate_p[i][j];
				p3[i + 3*j] = part.ypole_p[i][j];
				p4[i + 3*j] = part.ypole_rate_p[i][j];
				p5[i + 3*j] = part.ut1_rate_p[i][j];
			}
		}
	}

}